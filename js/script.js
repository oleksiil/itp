
window.addEventListener('DOMContentLoaded', () => {
});

// Btn to top

function scroll() {
    const scrolled = window.pageYOffset;
    const coords = document.documentElement.clientHeight / 4;

    if (scrolled > coords) {
        goTopBtn.classList.add('btn__top-show');
    }

    if (scrolled < coords) {
        goTopBtn.classList.remove('btn__top-show');
    }
}

function up() {
    const top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    let t;

    if (top > 0) {
        window.scrollBy(0, ((top + 100) / -10));
        t = setTimeout('up()', 20);
    } else clearTimeout(t);

    return false;
}

const goTopBtn = document.querySelector("[data-top='top']");

window.addEventListener('scroll', scroll);
goTopBtn.addEventListener('click', up);
